
# ISOFY
isofy — return non-ISO COBOL syntax to standard form

# SYNOPSIS
isofy [source ...]

# DESCRIPTION
isofy reads COBOL source files and mostly echoes the input to standard
output.  If no source is provided, isofy reads from standard input.

The only construct isofy looks for is extra parentheses in an Abbreviated
Complex Condition Relation.  It converts for example,

      A = ('1' OR '2' OR '3')

to

      A = '1' OR '2' OR '3'

isofy can also echo the changes to a log file.  See ENVIRONMENT.

# ENVIRONMENT
ISOFY   If the contents of the variable name a file that can opened
        (and created, if need be), isofy writes the input and output
        to that file, line by line.

# EXIT STATUS
The isofy utility exits 0 on success, and >0 if an error occurs. If
multiple files are named on the command line, it stops on the first
one it is unable to parse.

