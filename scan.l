/*
 * Copyright (c) 2023-2024 Symas Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following disclaimer
 *   in the documentation and/or other materials provided with the
 *   distribution.
 * * Neither the name of the Symas Corporation nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Flex overrides.
 */
%top {
    
#include "parse.h"

#include <assert.h>
#include <err.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

static void inline show_sc(); 

static bool cobol_filename( const char *name );
static const char * cobol_lineno_save();
static const char * cobol_filename_restore(); 

    extern int yychar;

static void yyerror( char const *s);

static void
yyerrorv( const char fmt[], ... ) {
  char *msg;
  va_list ap;

  va_start(ap, fmt);
  (void)! vasprintf(&msg, fmt, ap);
  assert(msg);
  yyerror(msg);
  free(msg);
  va_end(ap);
}



static void /* yynoreturn */ yy_fatal_error ( const char* msg  );

static void inline
die_fatal_error( const char msg[] ) {
  yyerrorv("fatal: %s",  msg);
  fflush(stderr);
  abort();
  yy_fatal_error(msg);
}

#define YY_FATAL_ERROR(msg) die_fatal_error((msg))

}

/*
 * Definitions.
 */

STRING [''][^''\n]+['']|[""][^""\n]+[""]
SPC  [[:space:]]+
BL   [[:blank:]]+
EOL  [[:blank:]]*r?\n

LP {SPC}?[(]{SPC}?
RP {SPC}?[)]{SPC}?

IS	IS|((IS{SPC}+NOT

LT <|LESS({SPC}THAN)?
LE {LT}{SPC}OR{SPC}{EQ}
EQ =|EQUALS?({SPC}TO)?
GE {GT}{SPC}OR{SPC}{EQ}
GT >|GREATER({SPC}THAN)?


WORD   [+[:alnum:]_-]+
OPER   [+=<>-]+
RELOP  (NOT{SPC})?({LT}|{LE}|{EQ}|{GE}|{GT})
TOKEN  ({WORD}|{OPER}){SPC}?

	    /* a "subscript" is any sequence of spaces and digits */
SUBSCRIPT [[:space:][:digit:]]+
REFMOD    {SUBSCRIPT}:{SUBSCRIPT}?

PUSH_FILE \f?[#]FILE{SPC}PUSH{SPC}.+\f
POP_FILE  \f?[#]FILE{SPC}POP\f

%{
#define return_token(T) do {			\
  yylval.string = strdup(yytext);		\
  return T;					\
} while(0);

int local_input( char buf[], int max_size, FILE *input );

#define YY_INPUT(buf, result, max_size)			\
{								\
  if( 0 == (result = local_input(buf, max_size, yyin)) )	\
    result = YY_NULL;						\
}

%}

%option debug noyywrap stack yylineno case-insensitive batch nodefault
			
%x procedure_div radar parsing
%%

		show_sc();

PROCEDURE{SPC}DIVISION[^.]*[.] { // start recognizing
          	  BEGIN(procedure_div); return_token(TEXT); }  


<procedure_div>{
  {RELOP}{SPC}?/[(]{SPC}?({WORD}|(ALL{SPC})?{STRING}){SPC}(AND|OR){BL}.* {
          	  yy_push_state(radar); return_token(TEXT); }  
}

<radar>{
  [(]/{SPC}?{WORD}{SPC}(AND|OR){SPC} {
        		   yy_push_state(parsing);
			   return_token(LP); }
  [(]/{SPC}?(ALL{SPC})?{STRING}{SPC}(AND|OR){SPC} {
        		   yy_push_state(parsing);
			   return_token(LP); }
  [^(].+  { yy_pop_state(); yyless(0); }
}

<parsing>{
  {LP}			{ yy_push_state(parsing);
			  return_token(LP); }
  {RP}			{ yy_pop_state();
			  return_token(RP); }

  OR/{SPC}?	{ return_token(OR); }
  AND/{SPC}? 	{ return_token(AND); }

  [+-]?[[:digit:]]+{SPC}?	{ return_token(NUMBER); }

  (ALL{SPC})?ZEROE?S?{SPC}? 	{ return_token(NUMBER); }
  (ALL{SPC})?SPACES?{SPC}? 	{ return_token(NUMBER); }
  (ALL{SPC})?LOW-VALUES?{SPC}? 	{ return_token(NUMBER); }
  (ALL{SPC})?HIGH-VALUES?{SPC}? { return_token(NUMBER); }
  (ALL{SPC})?QUOTES?{SPC}?	{ return_token(NUMBER); }
  (ALL{SPC})?NULLS?{SPC}? 	{ return_token(NUMBER); }

  {WORD}({SPC}?[(]({SUBSCRIPT}|{REFMOD})[)]){0,2}{SPC}? {
      			  return_token(WORD); }

  {WORD}{SPC}?		{ return_token(WORD); }
  {OPER}{SPC}?		{ return_token(WORD); }
  {OPER}{SPC}?		{ return_token(OPER); }
  {RELOP}{SPC}?		{ return_token(OPER); }

  (ALL{SPC})?{STRING}{SPC}? {
			  return_token(STRING); }

  {EOL}			{ return_token(SPACE); }
  {BL}			{ return_token(SPACE); }

  ^.{6}[*].*		{ return_token(SPACE); }
}

<*>{
  ^.{6}[*].*      { return_token(TEXT); } // fixed-form comment
  
  ^.{6}[[:blank:]]*{EOL} {		  // fixed-form blank line
	  		  yylval.string = strchr(yytext, '\n');
          	  	  return TEXT;
			} 
  
  {WORD}		{ return_token(TEXT); }
  {OPER}		{ return_token(TEXT); }
  {RELOP}		{ return_token(TEXT); }
  {STRING}		{ return_token(TEXT); }
  [(]{REFMOD}[)] 	{ return_token(TEXT); }
  [(]{SUBSCRIPT}[)] 	{ return_token(TEXT); }
  
  {EOL}			{ return_token(TEXT); }
  {BL}			{ return_token(TEXT); }
  [[:punct:]]      	{ return_token(TEXT); }
}

<procedure_div>{
    . { yyerrorv("procedure_div: not handled: 0x%02X, '%s'", *yytext, yytext); }
}

<*>{
  {PUSH_FILE}	{
			auto top_file = cobol_lineno_save();
			if( top_file ) {
			  if( yy_flex_debug ) warnx("   saving line %4d of %s",
						    yylineno, top_file); 
			}
			// "\f#file push <name>": name starts at offset 13.
			char *name = strdup(yytext);
			assert(name != NULL);
			name[yyleng - 1] = '\0'; // kill the trailing formfeed
			name = name + 12;
			cobol_filename(name);
			if( yy_flex_debug ) warnx(" starting line %4d of %s",
						  yylineno, name);
			printf("%s", yytext);
		}
  {POP_FILE} 	{
			auto name = cobol_filename_restore();
	   		if( yy_flex_debug ) warnx("restoring line %4d of %s", 
						  yylineno, name? name : "<none>");
			printf("%s", yytext);
		}
}

%%

#include <stack>
struct input_file_t {
  int lineno; const char *name;
  input_file_t( const char *name ) : lineno(1), name(name) {}
  bool operator==( const input_file_t& that ) const {
    return 0 == strcmp(name, that.name);
  }
};

const char *input_name  = "/dev/stdin";

static std::stack<input_file_t> input_filenames;

const char *
cobol_filename() {
  return input_filenames.empty()?
      input_name : input_filenames.top().name;
}

static bool
cobol_filename( const char *name ) {
  input_filenames.push(name);
  input_filenames.top().lineno = yylineno = 1;
  if( getenv(__func__) ) {
    warnx("   saving %s with lineno as %d",
	  input_filenames.top().name, input_filenames.top().lineno);
  }
  return true;
}

static const char *
cobol_lineno_save() {
  if( input_filenames.empty() ) return NULL;
  auto& input( input_filenames.top() );
  input.lineno = yylineno;
  if( getenv(__func__) ) {
    warnx("  setting %s with lineno as %d", input.name, input.lineno);
  }
  return input.name;
}
  
static const char *
cobol_filename_restore() {
  if( input_filenames.empty() ) return NULL;
  input_filenames.pop();
  if( input_filenames.empty() ) return NULL;

  auto input = input_filenames.top();
  yylineno = input.lineno;
  if( getenv("cobol_filename") ) {
    warnx("restoring %s with lineno to %d",
	  input_filenames.top().name, input.lineno);
  }
  return input.name;
}

static void
yyerror( char const *s) {
  fflush(stdout);
  if( yychar == 0 ) {  // strictly YYEOF, but not defined here
    fprintf( stderr, "%s:%d: %s detected at end of file\n",
             basename(strdup(cobol_filename())), yylineno, s);
    return;
  }

  if( !yytext || yytext[0] == '.') {
    fprintf( stderr, "%s:%d: %s\n",
             basename(strdup(cobol_filename())), yylineno, s);
    return;
  }

  auto len = yytext[yyleng-1] == '\n'? yyleng - 1 : yyleng;

  fprintf( stderr, "%s:%d: %s at '%.*s'\n",
           basename(strdup(cobol_filename())), yylineno, s, len, yytext);
}

      
static void inline show_sc() {
  if( yy_flex_debug ) 
    fprintf(stderr, "start condition %d:\t", YY_START);
}

#include <algorithm>
#include <cctype>
    
    /*
     * Read input line by line to fill the flex input buffer.  To
     * minimize whitespace pattern matching, do not copy trailing blanks.
     */

int
local_input( char buf[], int max_size, FILE *input ) {
  static_assert( sizeof(long) == sizeof(size_t), "fseeko required but not used" );
  static char buffer[YY_BUF_SIZE];
  long pos = ftell(input);
  char *line, *out = buf, *eob = buf + max_size;
  

  while( (line = fgets(buffer, sizeof(buffer), yyin)) != NULL ) {
    char *eol = line + strlen(line) - 1;

    if( line < eol && eol[-1] == '\r' ) {
      ////warnx( "removing CR 0x%02X", *eol );
      --eol;
    }

    if( line < eol && isblank(eol[-1]) ) {
      std::reverse_iterator<char*> rbeg(--eol);
      std::reverse_iterator<char*> rend(line);
      auto p = std::find_if( rbeg, rend,
                             []( char ch ) {
                               return ! isblank(ch);
                             } );
      p--;
      *p-- = '\n';
      *p-- = '\0';
    }
    
    size_t len( strlen(line) );

    // Next line would not fit; restore position.
    if( eob < out + len ) { 
      if( 0 != fseek(input, pos, SEEK_SET) ) {
        err(EXIT_FAILURE, "%s:%d", __func__, __LINE__);
      }
      return int(out - buf); // return filled buffer
    }

    memcpy(out, line, len);
    out += len;

    pos = ftell(input);
  }

  assert(line == NULL);
  assert(buf <= out);
  assert(out <= eob);
  
  if( ferror(input) ) err(EXIT_FAILURE, "%s:%d", __func__, __LINE__);

  return buf < out? int(out - buf) : YY_NULL;
}
