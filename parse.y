%token AND OR
%token	<string> LP RP OPER SPACE STRING TEXT WORD NUMBER

%type   <string> terms term term1 spaces
%type   <abbr_t> abbrs and_ors and_or

%code requires {
    #include <stdbool.h>
}
%union {
    int number;
    char *string;
    struct { bool has_ors; char *string; } abbr_t;
}

%printer { fprintf(yyo, "'%s'", $$.string ); } <abbr_t>
%printer { fprintf(yyo, "'%s'", $$ ); } <string>

%{
#include <err.h>
#include <stdio.h>

#include <algorithm>
#include <cstring>

extern int yylineno;

extern FILE *logfile;

    int yyerror(const char msg[]);
    int yylex(void);

    void Print( const char data[] ) {
	printf( "%s", data );
    }
    const char * cobol_filename();
%}

  // Use %defines instead of %header for Bison 3.5.2 compatibility. 
%defines "parse.h"
%define parse.trace

%%

top:		inputs
		;
inputs:		input
	|	inputs input
		;

input: 		TEXT		{ Print( $1 ); }
	|	parens
		;

parens:		LP[lp] abbrs RP[rp]
		{
		    char *text;
		    std::replace( $lp, $lp + strlen($lp), '(', ' ' );
		    std::replace( $rp, $rp + strlen($rp), ')', ' ' );
		    (void)!asprintf( &text, "%s%s%s", $1, $2.string, $3);
		    Print( text );
		    std::remove( text, text + strlen(text), '\n' );
		    warnx("%s:%d: removed parentheses: '%s'",
			  cobol_filename(), yylineno, text);
		}
	|	LP terms RP
		{
		    char *text;
		    (void)!asprintf( &text, "%s%s%s", $1, $2, $3);
		    Print( text );
		}
	|	terms LP[llp] LP[lp] abbrs RP[rp] RP[rrp] 
		{
		    char *text;
		    std::replace( $lp, $lp + strlen($lp), '(', ' ' );
		    std::replace( $rp, $rp + strlen($rp), ')', ' ' );
		    (void)!asprintf( &text, "%s %s%s%s%s %s",
				     $llp, 
				     $terms, $lp, $abbrs.string, $rp,
				     $rrp
				     );
		    Print( text );
		    std::remove( text, text + strlen(text), '\n' );
		    if( $abbrs.has_ors ) {
			warnx("%s:%d: warning: removed OR parentheses: '%s'",
			      cobol_filename(), yylineno, text);
		    } else {
			warnx("%s:%d: removed inner parentheses: '%s'",
			      cobol_filename(), yylineno, text);
		    }
		}
	|	LP terms abbrs RP
		{
		    char *text;
		    (void)! asprintf( &text, "( %s %s )",
				      $terms, $abbrs.string);
		    Print( text );
		}
		;

abbrs:		term and_ors  {
		    $$.has_ors = $2.has_ors;
		    (void)! asprintf( &$$.string, "%s %s",
				      $1, $and_ors.string); 
		}
		;

and_ors:	and_or  
	|	and_ors and_or
		{
		    $$.has_ors = $1.has_ors || $2.has_ors;
		    (void)! asprintf( &$$.string, "%s%s",
				      $1.string, $2.string); 
		}
		;

spaces:         SPACE
        |       spaces SPACE
                {
                    if( -1 == asprintf(&$$, "%s%s", $1, $2) ) {
                      err(EXIT_FAILURE, "so much space, so little memeory");
                    }
                    free($1);
                    free($2);
                }
                ;

and_or:		AND spaces term
		{
		    $$.has_ors = false;
		    (void)! asprintf( &$$.string, "AND %s%s", $2, $3); 
		}
	|	or spaces term
		{
		    $$.has_ors = true;
		    (void)! asprintf( &$$.string, "OR  %s%s", $2, $3); 
		}
		;

terms:		term
	|	terms term {
		    (void)! asprintf( &$$, "%s %s", $1, $term); 
		}
		;
term:		term1
		;

term1:		NUMBER
        |       OPER
        |       STRING
        |       WORD
                ;

or:		OR
	|	or spaces OR 
		;

