        PROGRAM-ID. PROG.
        DATA  DIVISION.
  *
      * Examples of nonstandard parenthetical expressions identified
      * by isofy.
        WORKING-STORAGE SECTION.
        77 T3163-COMP-CODE pic x.
        77 W3163-RQ-COMPANY pic x.
        77 TCMAA-RETURN-CODE-A pic 99.

        77 TCRAA-I-O-METHOD-A pic x.
        77 WFFEX-ERROR-CON-SERIES pic x.
        77 WFFEX-START-FIELD pic x.
        77 TFFCM-ITEM-DESC-A pic x.
        77 TETAA-FULL-TABLE-INDICATOR-A pic x.
        01 filler.
         02 elem occurs 55.
          05 TETAA-MESSAGE-DESCRIPTION-A pic x.
          05 TETAA-MESSAGE-DEPT-DESK-A pic x.
          05 TETAA-MESSAGE-CODE-A pic x.
          05 WAMED-CHECK-DIGIT-A pic x.
          05 TCRAA-OPEN-CLOSE-COMPANIES-A pic x.
          05 WADOP-COMPANY pic x.

        77 TETAA-MESSAGE-DPDESK-WORK-A pic x.
        77 TETAA-SPLIT-CHAR-A pic x.
        77 TETAA-MESSAGE-SPLIT-A pic x.
        77 WADOP-C-COUNT pic 9.
        77 WADOP-SUB pic 9.

        77 U20000-ERTB-EDIT-PASS pic x.
        77 TETAA-ERROR-SUBSCRIPT-A pic 9.
        77 WAMED-LOOK-SUBSCRIPT-A pic 9.
        77 WAMED-IS-IT-NEGATIVE-A pic x.

        77 WADMC-APCONN00-PARM1  usage pointer.
        77 TCRAA-COMPANY-RECORD-A pic x.

        77 COMMNTAI pic x.
        01  VAR1 PIC 999 DISPLAY.
        01 SHOULD_BE_N PIC 999.
        01 filler. 
           05 tab USAGE POINTER occurs 5.
           05 foo pic x(4).
           05 bar pic 9(4).

           05  WGDCL-MESSAGE.
               10  FILLER                      PIC X(08) VALUE
                   'WAIT ON:'.
               10  WGDCL-MESS-TRANS-CODES      PIC X(20).
               10  WGDCL-MESS-TRANS-CODE       REDEFINES
                   WGDCL-MESS-TRANS-CODES      OCCURS 4 TIMES.
                   15  WGDCL-TRANS-CODE        PIC X(4).
                   15  FILLER                  PIC X.
        77 MTRCO-PLAN-CODE-A pic x(12).

        77 MARAC-AMS-SITUATION-A pic X.
        77 MARAC-ENTRY-XDATE-A pic 9.
        77 MPFAA-ACCESS-KEY-A pic x.
        77 MPFAA-BENEFIT-TYPE-A pic x.
        77 MPFAA-ISSUE-AGE-A  pic x.
        77 MPFAA-MASTER-AREA-A pic x.
        77 MPFAA-PLAN-CODE-A pic x.
        77 MPFAA-PLAN-CODE-TYPE-A pic x.
        77 MPFAA-QUE-KEY-A pic x.
        77 MPFAA-SECTION-CODE-A pic x.
        77 MPFAA-SEX-A  pic x.
        77 MPFAA-THRU-DURATION-A  pic x.
        77 MPFAA-TYPE-OF-COVERAGE-A  pic x.
        77 TANST-DEATH-BEN-OPTION-A pic x.
        77 TESTR-RETURN-CODE-A pic X.
        77 TETAA-FORCE-TYPE-A pic x.
        77 TETAA-MESSAGE-CODE-WORK-A pic x(9).

        01 filler.
           02  TMFIO-KEYLENGTH-A pic x occurs 9.

        77 TMFIO-RECORD-PNTR-A POINTER.
        77 TMFIO-RETURN-CODE-A pic x.
        77 WGQCF-PLAA-ACCESS pic x.
        77 WGQCF-PLAA-AGE  pic x.
        77 WGQCF-PLAA-BENEFIT pic x.
        77 WGQCF-PLAA-COVERAGE  pic x.
        77 WGQCF-PLAA-DURATION  pic x.
        77 WGQCF-PLAA-FOUND pic x.
        77 WGQCF-PLAA-PLAN pic x.
        77 WGQCF-PLAA-QUE pic x.
        77 WGQCF-PLAA-SEX  pic x.
        77 WGQCF-PLAA-TYPE pic x.
        77 WRAST-P1-L13-PLAN pic x.
        77 WRAST-P1-L14-DB-OPTION pic x.
        77 WRAST-PLAN-DESC-16-30 pic x.
        77 WTXBR-KEY-POLICY pic x.
        77 WTXBR-KEY-SUB-TYPE pic x.
        77 WTXBR-KEY-TAX-YEAR pic x.
        77 WULPS-DATEW pic 9.

        77 WADMI-AAUMBROO PIC 9.
        77 WADMI-FH-IND-UPDATE PIC 9.
        77 WADMI-QUE-LENGTH PIC 9.

        77 TFHCA-SEND-FH-WIN-IND-A PIC 9.
        77 TCMAA-FIELD-HELP-ON   PIC 9.
           88  TOMAA-FIELD-HELP-ON-A value 7.
        77 TADMT-RETAIN-IND-W pic 9.

        LOCAL-STORAGE SECTION.
        EJECT
        01  VAR2 PIC 999 DISPLAY.
        01 SHOULD_BE_N1 PIC 999.
        PROCEDURE DIVISION.
        MOVE 123 TO VAR1
        MOVE 123 TO SHOULD_BE_N
        INITIALIZE VAR1 ALL VALUE
        DISPLAY VAR1 " should be " SHOULD_BE_N
        GOBACK.
        set tab(1) to NULLS.
        if foo = 'NOT' AND bar(1:3) = 'YES'
           display foo
        end-if.
        COMPUTE foo = bar / length of tab(2).

               IF  (MTRCO-PLAN-CODE-A (1:6)  =
                   '111A01'   OR  '111B01'   OR  '121A01'   OR  '121B01' 
                   OR  '111AG1'   OR  '111BG1'   OR  '121AG1'
                   OR  '111D01' OR  '121BG1'  OR  '111B03'
                   OR  '111B05'  OR  '111B07' OR  '111B10'
                   OR  '121B03'  OR  '121B05'  OR  '121B07'
                   OR  '121B10')
                    display 'yay'.

      * Sat Jan 13 14:37:08 2024
      * gcc version 13.2.1 20240110 (GCOBOL 13.0.16)
      * bison (GNU Bison) 3.7.4
      * flex 2.6.4
      * GNU Make 4.3

      * AGDGPL02.cbl:4254: error: 'SPACE' OR 'LOW_VALUES' invalid
      * because 'SPACE' is not a condition at 'OR'

4252            IF  TESTR-RETURN-CODE-A  =  '0'
4253                IF  MARAC-AMS-SITUATION-A  =
4254                    (SPACES  OR  LOW-VALUES  OR  ALL '.')
4255                    PERFORM  P30000-GET-SIT
4256                END-IF
4257            END-IF

      * ZPULPS91.cbl:2536: error: 'ZEROS' OR '99999999' invalid
      * because 'ZEROS' is not a condition at ')'

2536                IF MARAC-ENTRY-XDATE-A = (ZERO OR 99999999)
2537                   MOVE 0 TO WULPS-DATEW.
2538                IF WULPS-DATEW > 30

      * ZPRASTB1.cbl:3141: error: '1' OR 'B' invalid because '1' is
      * not a condition at ')'

3140            MOVE  WRAST-PLAN-DESC-16-30  TO  WRAST-P1-L13-PLAN.
3141            IF  TANST-DEATH-BEN-OPTION-A  = ('1'  OR  'B')
3142                MOVE  'OPTION 1'  TO  WRAST-P1-L14-DB-OPTION

      * APTXBR00.cbl:4103: error: 'SPACE' OR '.' invalid because
      * 'SPACE' is not a condition at 'OR'
      * APTXBR00.cbl:4106: error: 'SPACE' OR '..........' invalid
      * because 'SPACE' is not a condition at 'OR'
      * APTXBR00.cbl:4111: syntax error at 'ELSE'

4102            IF  WTXBR-KEY-TAX-YEAR  NUMERIC
4103                IF  WTXBR-KEY-SUB-TYPE  NOT =  (SPACES  OR  '.'  OR
4104                                                LOW-VALUES)
4105                    IF  WTXBR-KEY-POLICY  NOT =  (SPACES  OR
4106                                                 '..........'  OR
4107                                                  LOW-VALUES)
4108                        MOVE  +0018  TO  TMFIO-KEYLENGTH-A (1)
4109                    ELSE
4110                        MOVE  +0008  TO  TMFIO-KEYLENGTH-A (1)
4111                ELSE
4112                    MOVE  +0007  TO  TMFIO-KEYLENGTH-A (1)
4113            ELSE

      * APGQCF00.cbl:6459: error: 'WGQCF-PLAA-COVERAGE' OR '3' invalid
      * because 'WGQCF-PLAA-COVERAGE' is not a condition at ')'

                >>IF FOO is DEFINED
6446        IF  (TMFIO-RETURN-CODE-A  =  SPACES)
6447            SET  ADDRESS OF  MPFAA-MASTER-AREA-A  TO
6448                             TMFIO-RECORD-PNTR-A
6449            IF  ((MPFAA-ACCESS-KEY-A    =  WGQCF-PLAA-ACCESS)  AND
6450                 (MPFAA-QUE-KEY-A       =  WGQCF-PLAA-QUE)     AND
6451                 (MPFAA-PLAN-CODE-TYPE-A =  WGQCF-PLAA-TYPE)   AND
6452                 (MPFAA-PLAN-CODE-A     =  WGQCF-PLAA-PLAN)    AND
6453                 (MPFAA-BENEFIT-TYPE-A  =  WGQCF-PLAA-BENEFIT) AND
6454                 (MPFAA-SECTION-CODE-A  =  'AA')               AND
6455                 (MPFAA-ISSUE-AGE-A     =  WGQCF-PLAA-AGE)     AND
6456                 (MPFAA-THRU-DURATION-A  NOT <
6457                                  WGQCF-PLAA-DURATION)         AND
6458                 (MPFAA-TYPE-OF-COVERAGE-A  =
6459                                 (WGQCF-PLAA-COVERAGE OR '3')) AND
6460                 (MPFAA-SEX-A =  (WGQCF-PLAA-SEX      OR '3')))
6461                MOVE  'Y'       TO  WGQCF-PLAA-FOUND.
6462    P53000-PREPARE-TYPE-2.
                >>END-IF
      * APGQCF00.cbl:6914: error: '1' OR '2' invalid because '1' is
      * not a condition at 'OR'
      * APGQCF00.cbl:6916: syntax error at 'END-IF'

6912  *H2500SA
6913       IF  TETAA-MESSAGE-CODE-WORK-A (6:1)  =  'F'  AND
6914           TETAA-FORCE-TYPE-A  =  ('1' OR '2' OR '3')
6915           MOVE  'X'  TO  TETAA-MESSAGE-CODE-WORK-A (6:1)
6916       END-IF.
6917  *H2500EA

               IF  COMMNTAI  =  (ALL '.' OR LOW-VALUES OR SPACES)
                   NEXT SENTENCE
+++
      * Tue Feb  6 12:49:39 2024
       February-6 Section.
2830   010-READQ-TCRAA.
2831       MOVE  T3163-COMP-CODE        TO  W3163-RQ-COMPANY.
2832       MOVE  '1'                    TO  TCMAA-RETURN-CODE-A.
2833  *    EXEC CICS HANDLE CONDITION
      * ZP316302.cbl:3035: syntax error at '050-READ-CONTROL-REC'
3032       EXIT.
3033   EJECT.
3034  *****
3035   050-READ-CONTROL-REC.
3036  *    EXEC CICS READ
3037  *         DATASET(W3163-VALUES-FILE)

3923             if  TCRAA-I-O-METHOD-A  =  ('SQL '  OR  'DB2 ')
3924               SET  WADMC-APCONN00-PARM1  TO  ADDRESS OF
3925                    TCRAA-COMPANY-RECORD-A


1247  *    OCCUR ONLY IN THE FIRST SERIES.
1248  *
1249  *H3514SA
1250        IF  TFFCM-ITEM-DESC-A  =  ('GRBQ=' OR 'BQFF=')
1251  *H3514AD
1252  *     IF  TFFCM-ITEM-DESC-A  =  'GRBQ='
1253  *H3514ED
      * APFFEX00.cbl:1257: syntax error at 'END-IF'
1254           IF  WFFEX-ERROR-CON-SERIES  NOT  EQUAL  1
1255               MOVE  +13  TO  WFFEX-START-FIELD
1256           END-IF
1257        END-IF.
1258  *    FOR THE 'GRBK' FREEFORM, THE FIRST 4  FIELDS IN THE SPECS
1259  *    OCCUR ONLY IN THE FIRST SERIES.
1260  *
      * APFFEX00.cbl:1361: error: '1' OR '2' invalid because '1' is not a condition at 'OR'
1358  *
1359  *H2500SA
1360       IF  TETAA-MESSAGE-CODE-WORK-A (6:1)  =  'F'  AND
1361           TETAA-FORCE-TYPE-A  =  ('1' OR '2' OR '3')
1362           MOVE  'X'  TO  TETAA-MESSAGE-CODE-WORK-A (6:1)
1363       END-IF.
1364  *H2500EA
      * APFFEX00.cbl:1363: syntax error at 'END-IF'
1360       IF  TETAA-MESSAGE-CODE-WORK-A (6:1)  =  'F'  AND
1361           TETAA-FORCE-TYPE-A  =  ('1' OR '2' OR '3')
1362           MOVE  'X'  TO  TETAA-MESSAGE-CODE-WORK-A (6:1)
1363       END-IF.
1364  *H2500EA
1365       MOVE  SPACES  TO  TETAA-FULL-TABLE-INDICATOR-A.
1366       PERFORM  U10000-ERTB-CHECK-PASS
      * APFFEX00.cbl:1384: error: 'R' OR 'P' invalid because 'R' is not a condition at 'OR'
1381  *              TETAA-MESSAGE-DESCRIPTION-A (24)
1382           MOVE  TETAA-MESSAGE-DPDESK-WORK-A  TO
1383                 TETAA-MESSAGE-DEPT-DESK-A (24)
1384           IF  TETAA-SPLIT-CHAR-A  = ('R' OR 'P' OR 'S' OR 'E')
1385               MOVE  '09999R'  TO  TETAA-MESSAGE-CODE-A (24)
1386           ELSE
1387               PERFORM  U20000-ERTB-EDIT-PASS.
      * APFFEX00.cbl:1436: error: 'R' OR 'P' invalid because 'R' is not a condition at 'OR'
1433       U20000-ERTB-EDIT-PASS.
1434           MOVE  TETAA-MESSAGE-CODE-A (TETAA-ERROR-SUBSCRIPT-A)  TO
1435                 TETAA-MESSAGE-SPLIT-A.
1436           IF  TETAA-SPLIT-CHAR-A  =  ('R' OR 'P' OR 'S' OR 'E')
1437               MOVE  '1'  TO  TETAA-FULL-TABLE-INDICATOR-A
1438               ADD  24  TO  TETAA-ERROR-SUBSCRIPT-A.
1439       U20008-ERTB-EDIT-PASS-EXIT.
      * APFFEX00.cbl:1599: error: '+' OR 'LOW_VALUES' invalid because '+' is not a condition at ')'
1596  *   /
1597       U01100-CLEAR-POSITIVE.
1598           IF  WAMED-CHECK-DIGIT-A (WAMED-LOOK-SUBSCRIPT-A)  =
1599                                              ('+' OR LOW-VALUES)
1600               MOVE  ' '  TO
1601                     WAMED-CHECK-DIGIT-A (WAMED-LOOK-SUBSCRIPT-A)
1602           ELSE display 'hi'.
      * APFFEX00.cbl:1602: syntax error at 'ELSE'
1599           if 0 < 1 then        
1600               MOVE  ' '  TO
1601                     WAMED-CHECK-DIGIT-A (WAMED-LOOK-SUBSCRIPT-A)
1602           ELSE
1603         IF  WAMED-CHECK-DIGIT-A (WAMED-LOOK-SUBSCRIPT-A)  =  '-'
1604                   MOVE  'YES'  TO  WAMED-IS-IT-NEGATIVE-A.
        U10000-ERTB-CHECK-PASS.
            exit.
        P30000-GET-SIT.
          goback.

      * Wed Feb  7 15:05:08 2024
          P22100-LOAD-LOOP.
              IF TCRAA-OPEN-CLOSE-COMPANIES-A (WADOP-SUB) =
                 (SPACES OR WADOP-COMPANY (1))
                 NEXT SENTENCE
              ELSE
                  ADD 1 TO WADOP-C-COUNT
                  MOVE TCRAA-OPEN-CLOSE-COMPANIES-A(WADOP-SUB) TO
                  WADOP-COMPANY (WADOP-C-COUNT).

      * Mon Mar  4 12:57:47 2024
        S92000-CALL-AAUMBROO.
          display 1.
        592008-CALL-AAUMBROO-EXIT.
          display 2.

        IF TOMAA-FIELD-HELP-ON-A AND
           TFHCA-SEND-FH-WIN-IND-A 'Y'
        MOVE 'RTFH' TO WADMI-AAUMBROO
        PERFORM S92000-CALL-AAUMBROO
           THRU 592008-CALL-AAUMBROO-EXIT.
        IF WADMI-FH-IND-UPDATE OR (TOMAA-FIELD-HELP-ON-A AND
           TADMT-RETAIN-IND-W = '1')
        MOVE +5760 TO WADMI-QUE-LENGTH
        