YFLAGS = -v --report-file=parse.out --output=parse.c
YACC = bison
.PHONY: clean

LFLAGS = --batch
LEX = flex

CC = g++ -std=c++11
CFLAGS = -g -Og

GCOBOL = gcobol

isofy: isofy.o parse.o scan.o

scan.o: parse.c
isofy.o: parse.c

parse.c: parse.y
	$(YACC) $(YFLAGS) $^

clean: 
	@rm -fv *.o parse.[ch] scan.[ch] parse.out \
	t/*.iso t/*.cbl-ref t/*.o
test-clean:
	@rm -fv t/*.iso t/*.cbl-ref t/*.o


test: t/plethora.o isofy

iso:  t/plethora.iso

test.eg: isofy t/plethora.cbl
	$(GCOBOL) -dialect ibm -oo -preprocess ./isofy t/plethora.iso

test.eg2: isofy t/plethora.cbl
	cut -b-72 t/plethora.cbl | sed -E 's/^.{,6}/      /'	\
	| ./isofy  > t/plethora.iso
	$(GCOBOL) -dialect ibm -oo t/plethora.iso

#
# Patterns
#

%.c : %.y
	$(YACC) $(YFLAGS) $< 

%.cbl-ref : %.cbl
	cut -b-72 $< | sed -E 's/^.{,6}/      /' > $@

# Run isofy on Reference Format input.
# isofy requires the line-number area to be blanks and cannot discern
# where column 72 is. 
%.iso : %.cbl-ref
	./isofy $< > $@~
	@mv $@~ $@

# Run gcobol with an isofy preprocessor
%.o : %.cbl
	$(GCOBOL) -dialect ibm -c -o$@  -preprocess ./isofy $<

# Run gcobol with an isofied file
%.o : %.iso
	$(GCOBOL) -dialect ibm -c -o$@  $<
