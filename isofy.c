#include "parse.h"

#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

extern int yydebug, yy_flex_debug;
extern int yyleng, yychar, yylineno;
extern char *yytext;
extern FILE *yyin;

int yyparse(void);

extern const char *input_name;

int main(int argc, char *argv[])
{
  yydebug = yy_flex_debug = getenv("YYDEBUG")? 1 : 0;
  
  char *use_stdin = strdup(input_name);
  char **args = &use_stdin;
  if( 1 < argc ) {
    args = argv + 1;
    --argc;
  }

  for( int i=0; i < argc; i++ ) {
    input_name = args[i];
    if( (yyin = fopen(input_name, "r")) == NULL ) {
      err(EXIT_FAILURE, "could not open %s", input_name);
    }
    if( 0 != yyparse() ) {
      errx(EXIT_FAILURE, "%s failed on %s", argv[0], input_name);
    }
  }
  return EXIT_SUCCESS;
}

struct line_t { int cap; char *line; };

static bool
append( struct line_t *buf, const char input[] ) {
  assert(buf);
  assert(input);
  if( !buf->line || strlen(buf->line) + strlen(input) < buf->cap ) {
    char *s;
    buf->cap = asprintf(&s, "%s%s", buf->line, input);
    assert(s);
    free(buf->line);
    buf->line = s;
  } else {
    strcat(buf->line, input);
  }

  return NULL != strchr(buf->line, '\n');
}

int
println( FILE* file, const char input[], const char output[] ) {
  static struct line_t ibuf = {}, obuf = {};

  if( !input ) {
    fprintf(file, "end  input: %s\n", ibuf.line);
    fprintf(file, "end output: %s\n", obuf.line);
    return 0;
  }

  if( append(&ibuf, input) ) {
    fprintf(file, "%s:%d:  input: %s",
	    input_name, yylineno, ibuf.line);
    ibuf.line[0] = '\0';
  }
  if( append(&obuf, output) ) {
    fprintf(file, "%s:%d: output: %s",
	    input_name, yylineno, obuf.line);
    obuf.line[0] = '\0';
  }

  return 0;
}

const char *  cobol_filename();

void
yyerror( char const *s ) {
  char len = yytext[yyleng-1] == '\n'? yyleng - 1 : yyleng;
  fflush(stdout);
  if( yychar == 0 ) {  // strictly YYEOF, but not defined here
    warnx( "%s:%d: %s detected at end of file\n",
             cobol_filename(), yylineno, s );
    return;
  }
  
  if( yytext[0] == '.') {  
    warnx( "%s:%d: %s\n", cobol_filename(), yylineno, s );
    return;
  }

  warnx( "%s:%d: %s at '%.*s'\n",
           cobol_filename(), yylineno, s, len, yytext );
}
